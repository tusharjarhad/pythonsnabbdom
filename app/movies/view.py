from flask import Blueprint, jsonify, render_template

movies = Blueprint('movies', __name__, url_prefix='/counter')


@movies.route('/')
def get_movies():
    return render_template('counter.html')